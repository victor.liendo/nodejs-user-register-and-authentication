// entities is set to ["src/app/**/*.entity.js"] because this
// file is used only when deploying to AWS
export default {
  "name": "default",
  "type": "postgres",
  "host": "db host here ...",
  "port": 5432,
  "username": "db user name here",
  "password": "",
  "database": "db name here",
  "entities": ["src/app/**/*.entity.js"],
  "logging": false,
  "synchronize": false
}
