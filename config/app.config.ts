export default {
    jwtSecret: '@QEGTUI',
    // COGNITO USERS PARAMS
    cognitoPoolParams: {
        region: 'us-east-1',
	    cognitoUserPoolId: 'us-east-1_Ikb8QDqYY',
        tokenUse: 'access', // valid values are: access | id
        tokenExpiration: 3600000
    },
    // S3 PARAMS. Null values are replaced by .env values ...
    s3Params : {
        credentials: {
        accessKeyId: null,
        secretAccessKey: null,
        region: 'us-east-1',
        },
        bucketName: 'nacionsushi-backend'
    },
    // SIMPLE EMAIL SERVICE PARAMS. Null values are replaced by .env values ...
    sesParams: {
        host: 'email-smtp.us-east-1.amazonaws.com',
        port: 465,
        secure: true,
        auth: {
            user: null,
            pass: null
        }
    },
    // Amazon PERSONALIZE campaigns
    personalizeCampaigns : {
        users :  'arn:aws:personalize:us-east-1:529804162389:campaign/nacionsushi-campaign-00',
        items :  'arn:aws:personalize:us-east-1:529804162389:campaign/nacionsushi-campaign-01'
    },
    // RATES FOR RESIZING IMAGES
    imgDpis : {
        rates: {
            xxxhdpi : 1,
            xxhdpi : 0.75,
            xhdpi: 0.5,
            hdpi: 0.375,
            tvdpi: 0.3325,
            mdpi: 0.25,
            ldpi: 0.1875,
            x1: 0.25,
            x2: 0.5,
            x3: 0.75
            },
        count: 10
    },
    // Valid User Preferences
    userpreferences: ['country','restaurant'],
    // Valid Order types
    ordertypes: ['atrestaurant', 'delivery', 'carryout'],
    // Valid Order items
    orderitems: ['id', 'name', 'units', 'price', 'photo'],
    // Order Name Configuration
    ordername: 'NacionSushi RESTAURANTNAME Order Number: ORDERNUMBER',
    // Mapping reservation valid actions into reservation status
    reservationactions: {
            'confirm' : '1B',
            'assist' : '1D',
            'close': '2A',
            'cancelu' : '2B',
            'cancelr' : '2C',    
    },
    orderactions: {
            'process': '1C',
            'ready' : '1D',
            'serve' : '1E',
            'carryedout' : '1F',
            'sentocustomer' : '1G',
            'deliver': '1H',
            'payafter': '2B',
            'checkout': '2A'
    },   
    // CHALLENGE CODES EXPIRATION
    usercodes : {
        expiration : {
            PAYM : {
                time : 1,
                units : 'days'
            }
        }
    },
    apis : {
        EXECENV: {
            local : {
                misc : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/misc',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/misc'
                },
                users: {
                    public : 'http://localhost:3000/users',
                    admin : 'http://localhost:3001/users',
                },
                countries: {
                    public : 'http://localhost:3002/countries',
                    admin : 'http://localhost:3003/countries'
                },
                restaurants : {
                    public : 'http://localhost:3004/restaurants',
                    admin: 'http://localhost:3005/restaurants'
                },
		        productcategories : {
                    public : 'http://localhost:3008/productcategories',
                    admin: 'http://localhost:3009/productcategories'
                },
                products : {
                    public : 'http://localhost:3008/products',
                    admin: 'http://localhost:3009/products'
                },
                reservations : {
                    public : 'http://localhost:3010/reservations',
                    admin: 'http://localhost:3011/reservations'
                },
                orders : {
                    public : 'http://localhost:3012/orders',
                    admin: 'http://localhost:3013/orders'
                },
                api : {
                    public : 'http://localhost:3020/api',
                    admin : 'http://localhost:3021/api',
                },
                auth: {
                    admin : 'http://localhost:3023/auth',
                },
                employees: {
                    admin : 'http://localhost:3023/employees',
                },

            },
            awsdev: {
                misc : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/misc',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/misc'
                },
                users: {
                    public: 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/users',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/users'
                },
                countries: {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/countries',
                    admin : 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/countries'
                },
                restaurants : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/restaurants',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/restaurants'
                },
		        productcategories : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/productcategories',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/productcategories'
                },
                products : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/products',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/products'
                },
                reservations : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/reservations',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/reservations'
                },
                orders : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/orders',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/orders'
                },
                api : {
                    public : 'https://1zjsilgv78.execute-api.us-east-1.amazonaws.com/dev/api',
                    admin: 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/api'
                },
                auth: {
                    admin : 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/auth',
                },
                employees: {
                    admin : 'https://di4ex2a71l.execute-api.us-east-1.amazonaws.com/dev/employees',
                },
            }
        }
    }
};
