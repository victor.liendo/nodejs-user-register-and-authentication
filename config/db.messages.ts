export default {
        es: {
                gbl : {
                required : 'Dato requerido',
                size : 'Tamaño fuera de rango',
                emailbadformat : 'Formato inválido para email',
                numberbadformat : 'Formato inválido para un numero',
                datebadformat : 'Formato inválido para fecha',
                urlbadformat : 'Formato inválido para email',
                }
        },
        en : {
                gbl :
                {
                 required : 'Required data not supplied',
                 size : 'Field size out of range',
                 emailbadformat : 'Invalid format for an email field',
                 numberbadformat : 'Invalid format for a number field',
                 datebadformat : 'Invalid format for a date field',
                 urlbadformat : 'Invalid format for a URL field',
                }
        }
};
