
/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var authProyectotestamplify2bfd01e9UserPoolId = process.env.AUTH_PROYECTOTESTAMPLIFY2BFD01E9_USERPOOLID

Amplify Params - DO NOT EDIT */

// Gettin all the routes this API is going to handle
import routes from './src/app/routes';
// Loading express framework
const express = require('express');
// Making aur app able to parse the data coming in the REQUEST BODY
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
// Not used so far. Database connection will be handled through a function declared
// below
import {createConnection} from 'typeorm';
// Declaring headers that the client will be allowed to send to the server
import accessControlHeaders from './config/headers';
// Loading morgan for logging requests
const logger = require('morgan');
//const fileUpload = require('express-fileupload');
//
// Loading some environment values into "process.env"
//
require('dotenv').config();
//
// The main function: DB Connection and HTTP server init
//
import { MiscHelpers } from './src/app/helpers';
async function startapp() {
  // CREATE DATABASE CONNECTION
  // await createConnection();
  // await MiscHelpers.getDbConnection();
  // CREATE THE HTTP SERVER
  await app.listen(3023, function() {
    console.log('Employees ADMIN REST API available on port 3023');
  });
}
//
// Declare a new express app
//
const app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());
//
// Enabling file uploading
//
//app.use(fileUpload());
//
// Enabling logging using morgan with standard APACHE output + locale(LANG)
//
const requestAndResponsePropertiesToLog =
' :remote-addr - :remote-user [:date[clf]]"' +
' ":method :url HTTP/:http-version" :status :res[content-length]' +
' ":referrer" ":user-agent" ":req[accept-language]"';
//
// 1) Only in case we want to log only errors
//
// app.use(logger(requestAndResponsePropertiesToLog, {
//  skip: function (req, res) { return res.statusCode < 400; }
// }));

//
// 2) Enable LOGGING for all kind of reponsses
//
app.use(logger(requestAndResponsePropertiesToLog));
//
// Serving static files from the 'public' dir (example http://localhost:port/images/xxxhdpi/countries/panama.png)
//
//app.use(express.static(__dirname + '/public'));
//console.log(__dirname);
//
//
// Enable CORS for all methods
//
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', accessControlHeaders);
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});
//
// Set all routes from routes folder
//
app.use('/', routes);
//
// Start the app
//
startapp();
//
// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
// module.exports = app;
//
export default app;
