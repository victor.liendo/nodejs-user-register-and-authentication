import {MigrationInterface, QueryRunner} from "typeorm";

export class CrearEstructurasEmployee1580133192575 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "entitystatus_ety_usedby_enum" AS ENUM('User', 'Employee', 'Useridentity', 'Role')`);
        await queryRunner.query(`CREATE TABLE "entitystatus" ("ety_code" character varying(3) NOT NULL, "ety_description" character varying(128) NOT NULL, "ety_usedby" "entitystatus_ety_usedby_enum", "ety_createdat" TIMESTAMP NOT NULL DEFAULT now(), "ety_updatedat" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_216986f77ad63719c4372e9d719" PRIMARY KEY ("ety_code"))`);
        await queryRunner.query(`CREATE TABLE "roles" ("rol_id" integer NOT NULL, "rol_description" jsonb NOT NULL, "rol_createdat" TIMESTAMP NOT NULL DEFAULT now(), "rol_updatedat" TIMESTAMP NOT NULL DEFAULT now(), "rolStatusEtyCode" character varying(3) NOT NULL, CONSTRAINT "PK_3c527fdab46502238021eab2e22" PRIMARY KEY ("rol_id"))`);
        await queryRunner.query(`CREATE TABLE "employees" ("emp_id" SERIAL NOT NULL, "emp_email" character varying(128) NOT NULL, "emp_password" character varying, "emp_name" character varying(128), "emp_familyname" character varying(128), "emp_restaurant" integer, "emp_createdat" TIMESTAMP NOT NULL DEFAULT now(), "emp_updatedat" TIMESTAMP NOT NULL DEFAULT now(), "empRoleRolId" integer NOT NULL, "empCreatedbyEmpId" integer, "empUpdatedbyEmpId" integer, "empStatusEtyCode" character varying(3) NOT NULL, CONSTRAINT "UQ_ad1944750845d70fff2498fe37e" UNIQUE ("emp_email"), CONSTRAINT "PK_726544437a8b91e35c1734cf253" PRIMARY KEY ("emp_id"))`);
        await queryRunner.query(`CREATE TYPE "employeecodes_emc_category_enum" AS ENUM('EREG', 'PREC', 'PAYM', 'OTHER')`);
        await queryRunner.query(`CREATE TYPE "employeecodes_emc_durationunits_enum" AS ENUM('Mins', 'Hours', 'Days', 'Months', 'Years')`);
        await queryRunner.query(`CREATE TABLE "employeecodes" ("usc_id" SERIAL NOT NULL, "emc_code" character varying NOT NULL, "emc_category" "employeecodes_emc_category_enum" NOT NULL DEFAULT 'EREG', "emc_createdat" TIMESTAMP NOT NULL DEFAULT now(), "emc_updatedat" TIMESTAMP NOT NULL DEFAULT now(), "emc_duration" integer NOT NULL, "emc_durationunits" "employeecodes_emc_durationunits_enum" NOT NULL DEFAULT 'Days', "emc_status" boolean NOT NULL, "emcCreatedbyEmpId" integer NOT NULL, "emcUpdatedbyEmpId" integer NOT NULL, CONSTRAINT "UQ_40aa4cee180c6234d9248214691" UNIQUE ("emc_code"), CONSTRAINT "PK_0243879a13f39eba1eea2ef842a" PRIMARY KEY ("usc_id"))`);
        await queryRunner.query(`CREATE TYPE "tokens_tk_category_enum" AS ENUM('SESS', 'UREG', 'PREC', 'OTHER')`);
        await queryRunner.query(`CREATE TABLE "tokens" ("tk_id" SERIAL NOT NULL, "tk_token" character varying(256) NOT NULL, "tk_category" "tokens_tk_category_enum" NOT NULL DEFAULT 'SESS', "tk_useragent" character varying(128), "tk_status" boolean NOT NULL, "tk_createdat" TIMESTAMP NOT NULL DEFAULT now(), "tk_updatedat" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_d683689324760a1e50d7f53b2e3" UNIQUE ("tk_token"), CONSTRAINT "PK_9e64683addf0706ccf18ba1b66c" PRIMARY KEY ("tk_id"))`);
        await queryRunner.query(`ALTER TABLE "roles" ADD CONSTRAINT "FK_305d688ab02bc19276ab62dd43b" FOREIGN KEY ("rolStatusEtyCode") REFERENCES "entitystatus"("ety_code") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_ca3fdf26428f6a39e0f50c5c80b" FOREIGN KEY ("empRoleRolId") REFERENCES "roles"("rol_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_f229eee04b63cec027c9bb48208" FOREIGN KEY ("empCreatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_c7a05a08c054de86bf1c59ad0be" FOREIGN KEY ("empUpdatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_fdbd33d38d9a660f70c8acb0c6f" FOREIGN KEY ("empStatusEtyCode") REFERENCES "entitystatus"("ety_code") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employeecodes" ADD CONSTRAINT "FK_78941ad705f3b119af437c99611" FOREIGN KEY ("emcCreatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employeecodes" ADD CONSTRAINT "FK_cd2dc25ea091f0e0f2d828e4097" FOREIGN KEY ("emcUpdatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "employeecodes" DROP CONSTRAINT "FK_cd2dc25ea091f0e0f2d828e4097"`);
        await queryRunner.query(`ALTER TABLE "employeecodes" DROP CONSTRAINT "FK_78941ad705f3b119af437c99611"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_fdbd33d38d9a660f70c8acb0c6f"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_c7a05a08c054de86bf1c59ad0be"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_f229eee04b63cec027c9bb48208"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_ca3fdf26428f6a39e0f50c5c80b"`);
        await queryRunner.query(`ALTER TABLE "roles" DROP CONSTRAINT "FK_305d688ab02bc19276ab62dd43b"`);
        await queryRunner.query(`DROP TABLE "tokens"`);
        await queryRunner.query(`DROP TYPE "tokens_tk_category_enum"`);
        await queryRunner.query(`DROP TABLE "employeecodes"`);
        await queryRunner.query(`DROP TYPE "employeecodes_emc_durationunits_enum"`);
        await queryRunner.query(`DROP TYPE "employeecodes_emc_category_enum"`);
        await queryRunner.query(`DROP TABLE "employees"`);
        await queryRunner.query(`DROP TABLE "roles"`);
        await queryRunner.query(`DROP TABLE "entitystatus"`);
        await queryRunner.query(`DROP TYPE "entitystatus_ety_usedby_enum"`);
    }

}
