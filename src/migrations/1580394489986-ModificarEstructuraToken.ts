import {MigrationInterface, QueryRunner} from "typeorm";

export class ModificarEstructuraToken1580394489986 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TYPE "public"."entitystatus_ety_usedby_enum" RENAME TO "entitystatus_ety_usedby_enum_old"`);
        await queryRunner.query(`CREATE TYPE "entitystatus_ety_usedby_enum" AS ENUM('Employee', 'Role')`);
        await queryRunner.query(`ALTER TABLE "entitystatus" ALTER COLUMN "ety_usedby" TYPE "entitystatus_ety_usedby_enum" USING "ety_usedby"::"text"::"entitystatus_ety_usedby_enum"`);
        await queryRunner.query(`DROP TYPE "entitystatus_ety_usedby_enum_old"`);
        await queryRunner.query(`ALTER TABLE "tokens" DROP CONSTRAINT "UQ_d683689324760a1e50d7f53b2e3"`);
        await queryRunner.query(`ALTER TABLE "tokens" DROP COLUMN "tk_token"`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD "tk_token" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD CONSTRAINT "UQ_d683689324760a1e50d7f53b2e3" UNIQUE ("tk_token")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "tokens" DROP CONSTRAINT "UQ_d683689324760a1e50d7f53b2e3"`);
        await queryRunner.query(`ALTER TABLE "tokens" DROP COLUMN "tk_token"`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD "tk_token" character varying(256) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD CONSTRAINT "UQ_d683689324760a1e50d7f53b2e3" UNIQUE ("tk_token")`);
        await queryRunner.query(`CREATE TYPE "entitystatus_ety_usedby_enum_old" AS ENUM('User', 'Employee', 'Useridentity', 'Role')`);
        await queryRunner.query(`ALTER TABLE "entitystatus" ALTER COLUMN "ety_usedby" TYPE "entitystatus_ety_usedby_enum_old" USING "ety_usedby"::"text"::"entitystatus_ety_usedby_enum_old"`);
        await queryRunner.query(`DROP TYPE "entitystatus_ety_usedby_enum"`);
        await queryRunner.query(`ALTER TYPE "entitystatus_ety_usedby_enum_old" RENAME TO  "entitystatus_ety_usedby_enum"`);
    }

}
