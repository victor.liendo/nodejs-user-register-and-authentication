import {MigrationInterface, QueryRunner} from "typeorm";

export class ActualizarEntidadEmployee1580591854736 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_f229eee04b63cec027c9bb48208"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_c7a05a08c054de86bf1c59ad0be"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP COLUMN "empCreatedbyEmpId"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP COLUMN "empUpdatedbyEmpId"`);
        await queryRunner.query(`ALTER TABLE "employees" ADD "empCreatedbyIdentityEmpId" integer`);
        await queryRunner.query(`ALTER TABLE "employees" ADD "empUpdatedbyIdentityEmpId" integer`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_b50990bae77b09474f77e49f507" FOREIGN KEY ("empCreatedbyIdentityEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_9c3de8d8d4bab00f5247541e4da" FOREIGN KEY ("empUpdatedbyIdentityEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_9c3de8d8d4bab00f5247541e4da"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP CONSTRAINT "FK_b50990bae77b09474f77e49f507"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP COLUMN "empUpdatedbyIdentityEmpId"`);
        await queryRunner.query(`ALTER TABLE "employees" DROP COLUMN "empCreatedbyIdentityEmpId"`);
        await queryRunner.query(`ALTER TABLE "employees" ADD "empUpdatedbyEmpId" integer`);
        await queryRunner.query(`ALTER TABLE "employees" ADD "empCreatedbyEmpId" integer`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_c7a05a08c054de86bf1c59ad0be" FOREIGN KEY ("empUpdatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "employees" ADD CONSTRAINT "FK_f229eee04b63cec027c9bb48208" FOREIGN KEY ("empCreatedbyEmpId") REFERENCES "employees"("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
