
import { Role } from '../entities/role.entity';
import { Connection } from 'typeorm';
import { MiscHelpers } from '../helpers';

export abstract class RoleService {
   

    /**
     * findAll: Returns the list of all available ROLES for users of type "Employee"
     * @param lang: input lang for multilanguage
     *
    */
   static async findAll
   (
     lang : string
   ):
   Promise<object[]> {
     let roles;
     let dbConn: Connection = await MiscHelpers.getDbConnection(); 
     try {
       let columns = 'role.rol_id, ' +
                     'role.rol_description #> \'{' + lang + '}\' as rol_description';
       roles =  dbConn.createQueryBuilder().
       select(columns).
       from(Role, 'role');
       roles = roles.where('role.rol_status.ety_code = :code', {code: '11A'});   
      
       return roles.getRawMany();
 
     } catch (error) {
             console.log(error);
             return new Promise (function (resolve, reject) {
                 reject([]);
             });
     }
   }
    
}