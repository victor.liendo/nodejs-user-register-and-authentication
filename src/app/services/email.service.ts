const handlebars = require('handlebars');
const nodemailer = require('nodemailer');
import config from '../../../config/app.config';

import { challengecodeEmailTemplate,
       } from '../templates/';

// TODO:
// 1) Move the createTransport parameters outside the servicie, to something like a config file..
//    or include the createTransport parameters in app.properties ...
// 2) May be 'sender' should do 
// 3) There should be a different skeleton for each email, so they have to be implemented
//    as templates (in src/app/templates)
export abstract class EmailService {

    static async userChallengecodeMail(email: string, challengecode: string) {
        const sesParams = config.sesParams;
        sesParams.auth.user = process.env.SMTPUSER;
        sesParams.auth.pass = process.env.SMTPPASS;
        const transport = nodemailer.createTransport(sesParams);

        const sender = 'Nacion Sushi <aws@servitrackgps.com.ve>';
        const skeleton = challengecodeEmailTemplate;

        const template = handlebars.compile(skeleton);
        const data = { email : email, challengecode : challengecode };
        const content = template(data);

        const mailOptions = {
            from: sender,
            to: email,
            subject: 'Nacion Sushi: Código de Autorización',
            html: content,
            attachments: [
                {
                  filename: 'logo-nacion-sushi.png',
                  // path: './public/images/emails-attachments/logo-nacion-sushi.png',
                  path: './src/app/templates/logo-nacion-sushi.png',
                  cid: 'unique-logo-nacion-sushi.png'
                }
              ]
        };
        transport.sendMail(mailOptions, (error: any, info: { messageId: any; }) => {
            if (error) {
                console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
        });
    }
}
