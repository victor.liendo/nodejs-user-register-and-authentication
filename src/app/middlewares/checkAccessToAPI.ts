import { Request, Response, NextFunction } from 'express';
import config from '../../../config/app.config';
import { EndpointResponse, EndpointResponseStatus } from '../models';
import messages from '../../../config/app.messages';
import { MiscHelpers } from '../helpers';
const axios = require('axios');

export const checkAccessToAPI = async (req: Request, res: Response, next: NextFunction) => {
    //
    // Initializing HTTP response object
    //
    let endpointResponse = new EndpointResponse();
    //
    // Getting input data from the query string
    //
    const lang = req.query['lang'] ? req.query['lang'] : 'es';
    const appMessages = messages[lang];
    //
    let httpCode = 401;
    const propertiesWithErrors = [];
    //
    // Checking if the user in session is allowed to access this REST RESOURCE. For this to be done we must:
    // 1) Extract the requested REST RESOURCE from the request obtect:
    //          The following req attibute can be used to get the REST Resource:
    //          originalUrl : for example : /employees?lang=en  
    //                                      /api/getpermission?lang=en&scope=Admin&url=/employees/1&method=GET
    //          Hence, we must split by '?' so the complete rest REST resource is the first array element
    const requestUrl = req['originalUrl'].split('?');  // the 0 index holds the requested REST RESOURCE
    //
    // 2) Get permissions to access requestUrl[0] by calling the API Service
    let apiApiUrl = config.apis.EXECENV[process.env.EXECENV].api.admin
                    + '/getpermission?lang='
                    + lang
                    + '&scope=Admin'
                    + '&url='
                    + requestUrl[0]
                    + '&method=GET';

    // console.log(apiApiUrl);
    //
    // Calling the 'config' API. The response is an object with the same structure
    // as EndpointResponse, so we send it directly to the client without calling
    // MisHelpers.setResponse
    //
    const axiosConfig = {
                method: 'get',
                url: apiApiUrl,
                headers: { 'x-access-token': req.headers['x-access-token']}
    }  
    let axiosResponse = await axios(axiosConfig)
    .then(response => {
                 return response['data'];
    })
    .catch(error =>{
      let errMsg = 'CONFIG ADMIN API : ';
      if (error['response']) {
        // If the error was generated by the AUTH ADMIN API controller or service, there is an EndpointResponse
        // object coming in the response, from which we can extract the error message. Otherwise, we take
        // the error from error['response']
        if (error['response']['data']) {
          errMsg = errMsg + error['response']['data']['message'];
        } else {
          errMsg = errMsg + error['response']['statusText'];
        }     
        httpCode = error['response']['status'];
        propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
        return MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, errMsg);
      } else {
        httpCode = 500;
        errMsg = errMsg + error['code']
        propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
        return MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
      }
    });
    if (axiosResponse['code'] !== 200)  {
      res.status(axiosResponse['code']).send(axiosResponse);
      return;
    }
    //console.log(axiosResponse['data'][0]['resr_roles_involved']);
    //console.log(res.locals.user['role']);
    //
    // 3) Checking if the user's ROLE is in the array of allowed roles that's coming in the API response
    if (axiosResponse['data'][0]['resr_roles_involved'].indexOf(res.locals.user['role']) < 0) {
        httpCode=403;
        propertiesWithErrors.push({ property: 'none', errors: [appMessages.user_not_authorized]});
        endpointResponse=MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_not_authorized);
        res.status(httpCode).send(endpointResponse);
        return;
    }
    next();
};
