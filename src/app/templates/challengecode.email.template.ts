export const challengecodeEmailTemplate =
    '<html>' +
        '<head>' +
            '<style>' +
                'a { color: #FFFFFF; text-decoration: none}' +
            '</style>' +
        '</head>' +
        '<body>' +
            '<center>' +
                '<img src="cid:unique-logo-nacion-sushi.png" alt="nacionsushilogo"' +
            '</center>' +
            '<br>' +
            '<br>' +
            '<br>' +
            'El código solicitado para realizar la operación es:' +
            '<br>' +
            '<br>' +
            '<center>' +
                '<div style="font-size:x-large">' +
                '{{challengecode}}' +
                '</div>' +
            '</center>' +
            '<br>' +
            '<br>' +
            'Copyright XXXX-XXX Nacionsushi.com. Todos los derechos reservados' +
        '</body>' +
    '</html>';
