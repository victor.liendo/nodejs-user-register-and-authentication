import { Request, Response } from 'express';
import { RoleService } from '../services/role.service';
import messages from '../../../config/app.messages';
import { EndpointResponseStatus, EndpointResponse } from '../models';
import { MiscHelpers } from '../helpers';

//
// Notice:
// GET or PUT requests to non existent resources like /products/:id
// will throw an 404 http code. In this case, the response object
// will have the following structure
// { status:'ERROR', code:'404, error:  ,message:''}
//
// On the other hand ...
// GET requests to existing resources like /products or /products?name=pizza which
// returns an empty body (or data) will return 200
// In this case, the response object
// will have the following structure
// { status:'OK', code:'200', data:[]  ,message:''}
export class RolesController {

    static findAll = async (req: Request, res: Response) => {
      
      //
      // Initializing HTTP response object
      //
      let endpointResponse = new EndpointResponse();
      //
      // Getting input data from the query string
      //
      const lang = req.query['lang'] ? req.query['lang'] : 'es';
      const appMessages = messages[lang];
      //
      // Finding all Products
      //
      const propertiesWithErrors = [];
      const result =
      await RoleService.findAll(lang)
      .then(function getData(value: object[]) { return {status: 'OK', data: value}; })
      .catch(function getErrors() { return {status: 'FATAL', data: []}; });
      //
      // Checking results to select and send the HTTP Response
      //
      let httpCode = 200;
      switch (result['status']) {
        case 'OK': {
          if (result['data'].length <= 0) {
            // httpCode was set to '200' (instead of '404'),
            // data was set to '[]' (instead of 'null')
            // errors was set to null instead of 'propertiesWithErrors'
            // httpCode = 404;
            propertiesWithErrors.push({ property: 'none', errors: [appMessages.product_not_found]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, [], null, appMessages.product_not_found);
          } else {
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, result['data'], null, appMessages.product_found_ok);
          }
          break;
        }
        default: {
          httpCode = 500;
          propertiesWithErrors.push({ property: 'none', errors: [appMessages.gbl.fatal]});
          endpointResponse =
          MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
          break;
        }
      }
      res.status(httpCode).send(endpointResponse);
    }
}