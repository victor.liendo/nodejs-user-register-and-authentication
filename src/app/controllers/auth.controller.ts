import { Request, Response } from 'express';
import { AuthService } from '../services/auth.service';
import messages from '../../../config/app.messages';
import { EndpointResponseStatus,
         EndpointResponse,
         } from '../models';
import { MiscHelpers } from '../helpers';
import config from '../../../config/app.config';
import { Connection, AdvancedConsoleLogger } from 'typeorm';
import { ValidationHelpers } from '../helpers/validation-helpers';
import { Employee } from '../entities/employee.entity';
import { Category, Token } from '../entities/token.entity';


export class AuthController {
    /*
      login: Authenticates a User   
    */
    static login = async (req: Request, res: Response) => {
    //static login = async (req, res) => {
        console.log(req.body);
        //
        // Initializing  HTTP response object
        //
        let endpointResponse = new EndpointResponse();
        //
        // Getting input data from the query string or the URL
        //
        const lang = req.query['lang'] ? req.query['lang'] : 'es';
        const appMessages = messages[lang];
        const dbConn: Connection = await MiscHelpers.getDbConnection();
        let httpCode=200;
        const propertiesWithErrors = [];
        
        const authToken = null;    
        //
        // Initial validations
        //
        if (!req.body.email || req.body.email === 0) {
            httpCode=400;
            propertiesWithErrors.push({property: 'email', errors: [appMessages.gbl.bad_input_data]});
        } else {
            if (!ValidationHelpers.isAValidEmail(req.body.email)) {
                httpCode = 400
                propertiesWithErrors.push({property: 'email', errors: [appMessages.gbl.emailbadformat]});
            }
        }
        if (!req.body.password || req.body.password.length === 0) {
            httpCode = 400
            propertiesWithErrors.push({property: 'password', errors: [appMessages.gbl.bad_input_data]});
        }
        if (httpCode !== 200)
        {
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.bad_input_data);
            res.status(httpCode).send(endpointResponse);
            // Return is needed here to avoid the execution of the rest of the code
            return;
        }
        //
        // Now let's look for the user in the DB
        // 
        try {
            const employee = await dbConn.getRepository(Employee).findOne({ emp_email: req.body.email });
            if (!employee) {
                httpCode = 404;
                propertiesWithErrors.push({property: 'email', errors: [appMessages.user_not_found]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_not_found);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            if (employee.emp_status.ety_code !== '1A') {
                httpCode = 409;
                propertiesWithErrors.push({property: 'email', errors: [appMessages.user_not_active]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_not_active);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            const successFullLogin: boolean = await employee.comparePassword(req.body.password);
            if (!successFullLogin) {
                httpCode = 401;
                propertiesWithErrors.push({property: 'password', errors: [appMessages.user_wrong_password]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_wrong_password);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            //
            // User has been authenticated OK
            // We must generate the token to validate the user is authenticated for
            // incoming requests to access protected resources ...
            //
            endpointResponse = await AuthController.getToken(req, employee);
            httpCode=endpointResponse['code'];
        } catch (error) {
            httpCode = 500;
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
        }
        res.status(httpCode).send(endpointResponse);
    }

    /*
        verifyToken: Verifies a User's token
    */

    static verifyToken  = async (req: Request, res: Response) => {
        //
        // Initializing  HTTP response object
        //
        let endpointResponse = new EndpointResponse();
        //
        // Getting input data from the query string or the URL
        //
        const lang = req.query['lang'] ? req.query['lang'] : 'es';
        const appMessages = messages[lang];
        const propertiesWithErrors = [];
        let httpCode;
        //
        // Verifying the token coming in the request
        //
        if (!req.headers['x-access-token']) {
            httpCode = 400;
            propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.token_not_received]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, null, propertiesWithErrors, appMessages.token_not_received);
            res.status(httpCode).send(endpointResponse);
            return;
        }
        const x_access_token = req.headers['x-access-token'];
        const result = await AuthService.getDecodedToken(x_access_token)
        .then(function getData(value: object) { return value });
        let errMsg;
        httpCode = Number.parseInt(result['code']);
        switch (httpCode) {
            case 200: {
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, result['data']['decodedToken'], null, appMessages.token_ok);
                break;
            }
            case 409: {
                errMsg = appMessages.gbl.token_dec_failure;
                propertiesWithErrors.push({property: 'none', errors: [errMsg]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, errMsg);
                break;
            }
            case 403: {
                errMsg = appMessages.gbl.token_isnot_valid;
                propertiesWithErrors.push({property: 'none', errors: [errMsg]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
                break;
            }
            case 404: {
                errMsg = appMessages.gbl.token_not_found;
                propertiesWithErrors.push({property: 'none', errors: [errMsg]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
                break;
            }
            case 500: {
                errMsg = appMessages.gbl.fatal;
                propertiesWithErrors.push({property: 'none', errors: [errMsg]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
                break;
            }
        }
        res.status(httpCode).send(endpointResponse);
    }

    /*
        refreshToken: sends a new Token if required by the user
    */

    static refreshToken  = async (req: Request, res: Response) => {
        //
        // Initializing  HTTP response object
        //
        let endpointResponse = new EndpointResponse();
        //
        // Getting input data from the query string or the URL
        //
        const lang = req.query['lang'] ? req.query['lang'] : 'es';
        const appMessages = messages[lang];
        const dbConn: Connection = await MiscHelpers.getDbConnection();
        let httpCode;
        const propertiesWithErrors = [];
        try {
            const employee = await dbConn.getRepository(Employee).findOne({ emp_id: Number.parseInt(res.locals.user['id'])});
            if (!employee) {
                httpCode = 404;
                propertiesWithErrors.push({property: 'email', errors: [appMessages.user_not_found]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_not_found);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            if (employee.emp_status.ety_code !== '1A') {
                httpCode = 409;
                propertiesWithErrors.push({property: 'email', errors: [appMessages.user_not_active]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_not_active);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            //
            // we must invalidate the previous token before generating a new one
            //
            const dbToken = await dbConn.getRepository(Token).findOne({ tk_token: req.headers['x-access-token']});
            if (!dbToken) {
                httpCode = 404;
                propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.token_not_found]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, appMessages.gbl.token_not_found);
                res.status(httpCode).send(endpointResponse);
                return;
            }
            dbToken.invalidate();
            await dbConn.getRepository(Token).save(dbToken);
            endpointResponse = await AuthController.getToken(req, employee);
            httpCode=endpointResponse['code'];
           
        } catch (error) {
            console.log(error);
            httpCode = 500;
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
        }
        res.status(httpCode).send(endpointResponse);       
    }

    /*
        logout: Logs a user out of the system
    */
    static logout  = async (req: Request, res: Response) => {
        //
        // Initializing  HTTP response object
        //
        let endpointResponse = new EndpointResponse();
        //
        // Getting input data from the query string or the URL
        //
        const lang = req.query['lang'] ? req.query['lang'] : 'es';
        const appMessages = messages[lang];

        const dbConn: Connection = await MiscHelpers.getDbConnection();
        const propertiesWithErrors = [];
        let httpCode = 200;
        //
        // Verifying the token coming in the request
        //
        if (!req.headers['x-access-token']) {
            httpCode = 400;
            propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.token_not_received]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, null, propertiesWithErrors, appMessages.token_not_received);
            res.status(httpCode).send(endpointResponse);
            return;
        }

        const dbToken = await dbConn.getRepository(Token).findOne({ tk_token: req.headers['x-access-token']});
        if (!dbToken) {
            httpCode = 404;
            propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.token_not_found]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, appMessages.gbl.token_not_found);
            res.status(httpCode).send(endpointResponse);
            return;
        }
        try {
            dbToken.invalidate();
            await dbConn.getRepository(Token).save(dbToken);
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, {}, null, appMessages.user_loggedout_ok);
            res.status(httpCode).send(endpointResponse);
            return;
        } catch (error) {
            propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.fatal]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
        }
    }

    /*
        getToken: Generates a Token
    */

    static getToken = async (req: Request, employee: Employee) => {
        
        const lang = req.query['lang'] ? req.query['lang'] : 'es';
        const appMessages = messages[lang];
        let endpointResponse = new EndpointResponse();
        let httpCode = 200;
        const propertiesWithErrors = [];
        const payload = { 
            id: employee.emp_id,
            email: employee.emp_email,
            name: employee.emp_name,
            role: employee.emp_role.rol_id,
            restaurant: employee.emp_restaurant
        };

        const useragent = (req.headers['user-agent']) ? req.headers['user-agent'] : 'UNDEF';
        const   result = await AuthService.getToken(payload, Category.SESSION, useragent)
            .then(function getData(value: object) { return {status: 'OK', data: value}; })
            .catch(function getErrors() { return {status: 'FATAL', data: {} }; });             
        switch (result['status']) {
            case 'OK' : {
                result['data']['employee']= {
                            name: employee.emp_name,
                            familyname: employee.emp_familyname,
                            roleid: employee.emp_role.rol_id,
                            rolename: employee.emp_role.rol_description[lang],
                            restaurant: employee.emp_restaurant
                };
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, result['data'], null, appMessages.user_auth_ok);
                break;
            }
            default: {
                httpCode = 409;
                propertiesWithErrors.push({property: 'none', errors: [appMessages.gbl.token_gen_failure]});
                endpointResponse =
                MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors , appMessages.gbl.token_gen_failure);
                break;       
            }
        }
        return (endpointResponse);
    }
}
