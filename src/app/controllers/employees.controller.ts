import { Request, Response } from 'express';
//import { EmployeeService } from '../services/employee.service';
import { Employee } from '../entities/employee.entity';
import { Role } from '../entities/role.entity';
import messages from '../../../config/app.messages';
import { EndpointResponseStatus,
         EndpointResponse,
         } from '../models';
import { MiscHelpers } from '../helpers/misc-helpers';
import { ValidationHelpers } from '../helpers/validation-helpers';
import { Connection } from 'typeorm';
import { validate } from 'class-validator';
import { EmailService } from '../services/email.service';
import config from '../../../config/app.config';
import { Entitystatus } from '../entities';

//
// Notice:
// GET or PUT requests to non existent resources like /employees/:id
// will throw an 404 http code. In this case, the response object
// will have the following structure
// { status='ERROR', code='404, error:  ,message=''}
//
// On the other hand ...
// GET requests to existing resources like /employees or /employess?name=mary which
// returns an empty body (or data) will return 200
// In this case, the response object
// will have the following structure
// { status='OK', code='200', data:[]  ,message=''}

export class EmployeesController {

  static createEmployee = async (req: Request, res: Response) => {
    
    //
    // Initializing  HTTP response object
    //
    let endpointResponse = new EndpointResponse();
    //
    // Getting input data from the query string or the URL
    //
    const lang = req.query['lang'] ? req.query['lang'] : 'es';
    const appMessages = messages[lang];
    const dbConn: Connection = await MiscHelpers.getDbConnection();
    let httpCode = 201;
    const propertiesWithErrors = [];
    // First validation we must do is checking that the new user does not exist
    let employee = await dbConn.getRepository(Employee).findOne({emp_email: req.body.email});
    if (employee) {
      httpCode = 409;
      propertiesWithErrors.push({property: 'email', errors: [appMessages.user_already_registered]});
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_already_registered);
      res.status(httpCode).send(endpointResponse);
      // Return is needed here to avoid the execution of the rest of the code
      return;
    }
    employee = new Employee();
    //
    // GET employee DATA FROM THE REQUEST BODY
    // CREATE A NEW employee OBJECT, then VALIDATE IT
    //
    employee.emp_email = req.body.email;
    employee.emp_password = req.body.password;
    employee.emp_name = req.body.name;
    employee.emp_familyname = req.body.familyname;
    employee.emp_restaurant = req.body.restaurant;
  
    //
    // We inspect the errors array or vector, to see which properties have errors
    //
    let errors = await validate(employee);
    if (errors.length > 0) {
      errors.forEach(employeePropertyWithErrors => {
          const errorDetails = [];
          for ( const key in employeePropertyWithErrors['constraints']) {
            if ( employeePropertyWithErrors['constraints'].hasOwnProperty(key) ) {
                errorDetails.push(employeePropertyWithErrors['constraints'][key]);
            }
          }

          propertiesWithErrors.push({property: employeePropertyWithErrors['property'], errors: errorDetails});
      });
      httpCode = 400;
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.bad_input_data);
      res.status(httpCode).send(endpointResponse);
      // Return is needed here to avoid the execution of the rest of the code
      return;
    }
    //
    // Request body has overcome initial validations. Let's keep applying additional
    // validation rules
    //
    // Initial role checking
    //
    if (!req.body.role) {
      propertiesWithErrors.push({property: 'role', errors: [appMessages.gbl.bad_input_data]});
      httpCode = 400;
    } else {
        if (isNaN(req.body.role)) {
          propertiesWithErrors.push({property: 'role', errors: [appMessages.gbl.bad_input_data]});
          httpCode = 400;
        } 
    }
    // Checking the email format
    if (!ValidationHelpers.isAValidEmail(req.body.email)) {
        propertiesWithErrors.push({property: 'email', errors: [appMessages.gbl.emailbadformat]});
        httpCode = 400;
    }
    // Checking the password
    if (!ValidationHelpers.isAValidPassword(req.body.password)) {
        httpCode = 400
        propertiesWithErrors.push({property: 'password', errors: [appMessages.gbl.passwordbadformat]});
    }
    if (httpCode !== 201)
    {
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.bad_input_data);
      res.status(httpCode).send(endpointResponse);
      // Return is needed here to avoid the execution of the rest of the code
      return;
    }
    //
    // So far, every field has overcome the validation rules
    // Let's check that a valid role has been set for this employee
    let role = await dbConn.getRepository(Role).findOne({rol_id: Number.parseInt(req.body.role)});
    if (!role) {
      httpCode = 409;
      propertiesWithErrors.push({property: 'role', errors: [appMessages.role_isnot_valid]});
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.role_isnot_valid);
      res.status(httpCode).send(endpointResponse);
      // Return is needed here to avoid the execution of the rest of the code
      return;
    } else {
      // The role of the employee that is been created cannot be greater than the role of the
        // employee in session. For example, a RESTAURANT ADMINISTRATOR cannot CREATE A SYSTEM
        // ADMINISTRATOR
        if (Number.parseInt(req.body.role) > Number.parseInt(res.locals.user['role'])) {
            httpCode = 409;
            propertiesWithErrors.push({property: 'role', errors: [appMessages.user_cannot_createuser]});
            endpointResponse =
            MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.user_cannot_createuser);
            res.status(httpCode).send(endpointResponse);
            return;
          }
    }
    employee.emp_role = role;
    const employeestatus = new Entitystatus();
    employeestatus.ety_code='1B';
    employee.emp_status = employeestatus;
    await employee.setPassword(req.body.password);
    if (employee.emp_password === 'UNDEF') {
      httpCode = 400;
      propertiesWithErrors.push({property: 'password', errors: [appMessages.gbl.fatal]});
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
      res.status(httpCode).send(endpointResponse);
      // Return is needed here to avoid the execution of the rest of the code
      return;
    }
    // An aditional validation must be carried out: the employee must be created only
    // for a valid restaurant
    //
    
    // Ready to save the new employee
    //
    try {
      await dbConn.getRepository(Employee).save(employee);
      // JUST TESTING THAT the input password can be used for login
      // const successFullLogin: boolean = await employee.comparePassword(req.body.password);
      // console.log(successFullLogin);
      employee.emp_password='UNDEF';
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.OK, httpCode, employee, null, appMessages.user_saved_ok);
    } catch (error) {
      httpCode = 500;
      endpointResponse =
      MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, appMessages.gbl.fatal);
    }
    res.status(httpCode).send(endpointResponse);
  }

  static findOneById = async (req: Request, res: Response) => {
    res.status(200).send('OK');
  }
}
