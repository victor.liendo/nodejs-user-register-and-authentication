import { Router } from 'express';
import { EmployeesController } from '../controllers/employees.controller';
import { checkJWT } from '../middlewares/checkJWT';
import { checkAccessToAPI } from '../middlewares/checkAccessToAPI';

const router = Router();

// Add a new Employee
// router.post('/', [checkJWT, checkRole], EmployeesController.createEmployee);
router.post('/', [checkJWT, checkAccessToAPI], EmployeesController.createEmployee);
router.get('/:id', [checkJWT, checkAccessToAPI], EmployeesController.findOneById);
export default router;

