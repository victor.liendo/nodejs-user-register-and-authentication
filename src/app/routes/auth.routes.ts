import { Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { checkJWT } from '../middlewares/checkJWT';

const router = Router();

// Authentication route
router.post('/login', AuthController.login);
// Token verification route
router.get('/verifytoken', AuthController.verifyToken);
// Token refreshing route
router.put('/refreshtoken', [checkJWT], AuthController.refreshToken);
// Log out route
router.put('/logout', [checkJWT], AuthController.logout);
export default router;
