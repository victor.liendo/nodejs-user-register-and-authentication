import { Router } from 'express';

// Importing routes from each routes file. I guess that
// the default object, variable or function exported by
// 'countries.routes.ts' file (called 'router' in this case, and only
// one object...) is assigned to the 'countryroutes' variable.
// Thus, it can have a different name in both the exporting
// and exporting file

import employeesroutes from './employees.routes';
import authroutes from './auth.routes';
import roleroutes from './role.routes';

const router = Router();

router.use('/employees',  employeesroutes);
router.use('/roles', roleroutes);
router.use('/auth', authroutes);

export default router;
