import { Router } from 'express';
import { RolesController } from '../controllers/roles.controller';
import { checkJWT } from '../middlewares/checkJWT';
import { checkAccessToAPI } from '../middlewares/checkAccessToAPI';

const router = Router();

// Add a new Employee
router.get('/', [checkJWT, checkAccessToAPI], RolesController.findAll);
export default router;