
// This class is intended to standarize the reponses for all the endpoints

export enum EndpointResponseStatus {
  OK = 'OK',
  ERR = 'ERROR',
  FATAL = 'FATAL'
}

export class EndpointResponse
{
  status: EndpointResponseStatus;
  code: number;
  data: object;
  error: object;
  message: string;

  setStatus(status: EndpointResponseStatus): void {
    this.status = status;
  }

  setCode(code: number): void {
    this.code = code;
  }

  setData(data: object): void {
    this.data = data;
  }

  setErrors(error: object): void {
    this.error = error;
  }

  setMessage(message: string): void {
    this.message = message;
  }

  isOk(): boolean {
    if (this.status === EndpointResponseStatus.OK) {
      return true;
    } else {
      return false;
    }
  }
}
