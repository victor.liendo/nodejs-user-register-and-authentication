// @ts-ignore : 'Column' is declared but its value is never read.
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
// const moment = require('moment');

export enum Category {
  SESSION = 'SESS',          // User Registration
  REGISTRATION = 'UREG',
  PASSRECOVERY = 'PREC',     // Password Recovery
  OTHER = 'OTHER'            // Not used yet
}

@Entity({name: 'tokens'})
/** Class Token: to store tokens sent to the client app  */
export class Token
{

  @PrimaryGeneratedColumn()
  tk_id: number;

  @Column({ unique: true })
  tk_token: string;

  @Column({
    type: 'enum',
    // tslint:disable-next-line: object-literal-sort-keys
    enum: Category,
    default: Category.SESSION,
    nullable: false
  })
  tk_category: Category;

  @Column({ length:  128, nullable: true})
  tk_useragent: string;

  @Column()
  tk_status: boolean;

  @CreateDateColumn()
  tk_createdat: Date;

  @UpdateDateColumn()
  tk_updatedat: Date;

  /**
   * No input params
   * @returns: void. The token is invalidated in the database
   */
  async invalidate() {
    // const updatedat = moment(new Date());
    this.tk_status = false;
  }

}