
// @ts-ignore : 'Column' is declared but its value is never read.
import {  Column,
          CreateDateColumn,
          Entity,
          ManyToOne,
          PrimaryGeneratedColumn,
          UpdateDateColumn } from 'typeorm';
// const moment = require('moment');
import { Employee } from '../entities/employee.entity';
import { Length,
         IsNotEmpty,
         IsNumber, 
       } from "class-validator";
import messages from '../../../config/db.messages';
const moment = require('moment');

export enum Category {
  REGISTRATION = 'EREG',   // Employee Registration
  PASSRECOVERY = 'PREC',   // Password Recovery
  PAYMENTTRANS = 'PAYM',   // Payment Transacc
  OTHER = 'OTHER'          // Not used yet
}

export enum Dunits {
  MIN = 'Mins',
  HRS = 'Hours',
  DYS = 'Days',
  MTH = 'Months',
  YRS = 'Years'
}

@Entity({name: 'employeecodes'})
/** Class Token: to store tokens sent to the client app  */
export class Employeecode
{

  @PrimaryGeneratedColumn()
  usc_id: number;

  @IsNotEmpty({message: messages.es.gbl.required + ' [' + messages.en.gbl.required + ']'})
  @Column({ unique: true })
  emc_code: string;

  @Column({
    type: 'enum',
    // tslint:disable-next-line: object-literal-sort-keys
    enum: Category,
    default: Category.REGISTRATION,
    nullable: false
  })
  emc_category: Category;

  @CreateDateColumn()
  emc_createdat: Date;

  @ManyToOne(type => Employee, { nullable: false, eager: false})
  emc_createdby: Employee;

  @UpdateDateColumn()
  emc_updatedat: Date;

  @ManyToOne(type => Employee, { nullable: false, eager: false})
  emc_updatedby: Employee;

  @Column({nullable: false})
  emc_duration: number;

  @Column({
    type: 'enum',
    // tslint:disable-next-line: object-literal-sort-keys
    enum: Dunits,
    default: Dunits.DYS,
    nullable: false
  })
  emc_durationunits: Dunits;


  @Column()
  emc_status: boolean;

  /**
   * No input params
   * @returns: void. The token is invalidated in the database
   */
  async invalidate() {
    // const updatedat = moment(new Date());
    this.emc_status = false;
  }

  hasExpired(): boolean {
    // TypeORM returns database dates in UTC. It results in those  dates to be in a different TZ (timezone)
    // the servers's TZ. Hence, we move database dates to the server TZ by substracting an offset in minutes
    let expired = true;
    const createdAt = new Date(moment(this.emc_createdat).subtract(this.emc_createdat.getTimezoneOffset(), 'minutes'));
    switch (this.emc_durationunits) {
      case Dunits.MIN : {
        // We add a number of mins
        createdAt.setMinutes(createdAt.getMinutes() + this.emc_duration);
        break;
      }
      case Dunits.HRS : {
        // We add a number of hours
        createdAt.setHours(createdAt.getHours() + this.emc_duration);
        break;
      }
      case Dunits.DYS : {
        // We add a number of days
        createdAt.setDate(createdAt.getDate() + this.emc_duration);
        break;
      }
      case Dunits.MTH : {
        createdAt.setMonth(createdAt.getMonth() + this.emc_duration);
        break;
      }
      default: {
        createdAt.setFullYear(createdAt.getFullYear() + this.emc_duration);
        break;
      }
    }
    const dateNow = new Date(moment());
    if (dateNow < createdAt) {
        expired = false;
    }
    return expired;
  }
}
