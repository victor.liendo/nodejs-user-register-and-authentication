import { BaseEntity,
         Column,
         Entity,
         ManyToOne,
         PrimaryGeneratedColumn,
         CreateDateColumn,
         UpdateDateColumn,
         } from 'typeorm';

import { Entitystatus } from './entitystatus.entity';
import messages from '../../../config/db.messages';
import { IsNotEmpty, Length} from 'class-validator';
import { Role } from './role.entity';
var bcrypt = require('bcrypt');

@Entity({name: 'employees'})
/** Class User: to store system's users and their attributes  */
export class Employee extends BaseEntity {

  @PrimaryGeneratedColumn()
  emp_id: number;

  @IsNotEmpty({message: messages.es.gbl.required + ' [' + messages.en.gbl.required + ']'})
  @Length(1, 128, {message: messages.es.gbl.size + ' [' + messages.en.gbl.size + ']'})
  @Column({length: 128, nullable: false, unique: true})
  emp_email: string;

  @IsNotEmpty({message: messages.es.gbl.required + ' [' + messages.en.gbl.required + ']'})
  @Length(8, 10, {message: messages.es.gbl.size + ' [' + messages.en.gbl.size + ']'})
  @Column({nullable: true})
  emp_password: string;

  @IsNotEmpty({message: messages.es.gbl.required + ' [' + messages.en.gbl.required + ']'})
  @Length(1, 128, {message: messages.es.gbl.size + ' [' + messages.en.gbl.size + ']'})
  @Column({length: 128, nullable: true})
  emp_name: string;

  @IsNotEmpty({message: messages.en.gbl.required + ' [' + messages.es.gbl.required + ']'})
  @Length(1, 128, {message: messages.en.gbl.size + ' [' + messages.es.gbl.size + ']'})
  @Column({length: 128, nullable: true})
  emp_familyname: string;

  @IsNotEmpty({message: messages.en.gbl.required + ' [' + messages.es.gbl.required + ']'})
  @Column({nullable: true})
  emp_restaurant: number;

  // We need to set eager=true, because the Auth (login) controller need to
  // set the employee's Role into the JWT Token
  @ManyToOne(type => Role, { nullable: false, eager: true})
  emp_role: Role;

  @CreateDateColumn()
  emp_createdat: Date;

  @ManyToOne(type => Employee, { nullable: true, eager: false})
  emp_createdby_identity: Employee;

  @UpdateDateColumn()
  emp_updatedat: Date;

  @ManyToOne(type => Employee, { nullable: true, eager: false})
  emp_updatedby_identity: Employee;

  // We need to set eager=true, because the Auth (login) controler need to check the User status;
  @ManyToOne(type => Entitystatus, { nullable: false, eager: true})
  emp_status: Entitystatus;
  /**
   * @param: password (plaint text)
   * @returns: void. The input password is encrypted and stored in the DB
   *
   */
  async setPassword(password: string) {
    const BCRYPT_SALT_ROUNDS = 12;
    const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS)
    .then(function(hashedPassword) {
        return hashedPassword;
    }).
    catch(function(error){
        return 'UNDEF';
    });
    this.emp_password=hashedPassword;
  }

  async comparePassword (password: string) {
    return bcrypt.compare(password, this.emp_password);
  }

  async setStatus(status: Entitystatus) {
    this.emp_status = status;
  }
}
