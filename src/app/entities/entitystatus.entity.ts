// @ts-ignore : 'Column' is declared but its value is never read.
import { Column, Entity, PrimaryGeneratedColumn, Code, PrimaryColumn, CreateDateColumn, UpdateDateColumn} from 'typeorm';

export enum Usedby {
  EMPLOYEE = 'Employee',
  ROLE = 'Role'
}

@Entity({name: 'entitystatus'})
/** Class Entitystatus: to store STATUS for the rest of the entities  */
export class Entitystatus {

  @PrimaryColumn ({length: 3, nullable:false})
  ety_code: string;           // This will be a 1 digit, 1 letter code for representing a naming space
                          // for each entity status codes
  @Column({length: 128})
  ety_description: string;

  @Column({
    type: 'enum',
    // tslint:disable-next-line: object-literal-sort-keys
    enum: Usedby,
    nullable: true
  })
  ety_usedby: Usedby;

  @CreateDateColumn()
  ety_createdat: Date;

  @UpdateDateColumn()
  ety_updatedat: Date;
}
