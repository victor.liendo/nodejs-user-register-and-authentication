import { Column, Entity, PrimaryColumn, CreateDateColumn, UpdateDateColumn, ManyToOne} from 'typeorm';
import { Entitystatus } from './entitystatus.entity';

@Entity({name: 'roles'})
/* Class Role
   To establish the different types of roles a user can have...
   For users of type 'CUSTOMER', no role will be attached to
   the DB or COGNITO instance. For the rest of the users, a
   custom attribute must be defined in COGNITO to store the
   user role
*/
export class Role {
  //
  @PrimaryColumn ()
  rol_id: number;           
  //
  // This will hold the ROLE description, both in English and Spanish
  //
  @Column({type: "jsonb", nullable: false})
  rol_description: Object;

  @CreateDateColumn()
  rol_createdat: Date;

  @UpdateDateColumn()
  rol_updatedat: Date;

  @ManyToOne(type => Entitystatus, { nullable: false, eager: false})
  rol_status: Entitystatus;
}
