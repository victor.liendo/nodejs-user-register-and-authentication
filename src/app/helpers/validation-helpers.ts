/**
 *  This class is intended to provide validation helpers
 */

export abstract class  ValidationHelpers {

    /**
     *  isAValidEmail:
     *
     *
     *  \S+ Matches a any non space character (a letter, number, special character) any number of times
     *     (this pattern is for the first part of an email)
     *
     *          followed by an
     *
     *  @  (next pattern)
     *
     *          followed by
     *
     *  \S (next pattern, the same as first)
     *
     *          followed by
     *
     *  . (next pattern)
     *
     *          followed by
     *
     *  \S (last pattern, the same as first)
     *
     */
    static isAValidEmail(email: string): boolean {
        var re = /\S+@\S+\.\S+/;
        return re.test(String(email).toLowerCase());
    }

     /**
     *  isAValidEmail
     *  a secuence of only digits, minimum seven times up to 15 times
     */
    static isAValidPhoneNumber(phone: string): boolean {
       var re = /^[0-9]{7,15}$/;
       return re.test((phone));
    }

    static isAValidPassword(password: string): boolean {
        // ^ The password string will start with a character belonging to any of the groups
        // (?=.*[a-zA-Z])	The string must contain at least 1 lowercase or uppercase alphabetical character
        // (?=.*[0-9])	The string must contain at least 1 numeric character
        // (?=.*[@$&%#\*])	The string must contain at least one special character, but we are escaping *
        // because it is reserved for RegExp
        // The string must be an 8 characters string
        
        var re = new RegExp("^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[@$&%#\*])([a-zA-Z0-9@$&%#\*]){8,8}$");

        return re.test(password);
    }
}
