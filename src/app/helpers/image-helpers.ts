import { EndpointResponseStatus, EndpointResponse } from '../models';
const sharp = require('sharp');
const AWS = require('aws-sdk');
import config from '../../../config/app.config';
import { Connection, createConnection, ConnectionManager, getConnectionManager } from 'typeorm';

/**
 *  This class is intended to provide misc helpers
 *
 */



export abstract class  ImageHelpers {


    /**
     * savePictures: Takes an 'entity' picture, resize it to different sizes and
     *               put them into the corresponding 'entity' folder in a S3 bucket
     *
     * @param entity: a string containing the corresponding entity (countries, products, etc)
     * @param picture: A Buffer object containing the image data
     * @param width: The input image's width
     * @param height: the input image's height
     * @returns: an Array containg object of the kind {resolution: 'string', url: 'string'}
     */

    static async savePictures(entity: string, picture: object, width: number, height: number): Promise<object> {
        // Logging the input image and the sharp supported formats
        // console.log(sharp.format);
        // console.log(picture);
        //
        //
        //  Initializing  S3
        //
        const s3Credentials = config.s3Params.credentials;
        s3Credentials.accessKeyId = process.env.ACCESSKEY;
        s3Credentials.secretAccessKey = process.env.SECRETKEY;
        const s3 = new AWS.S3(s3Credentials);
        // console.log(s3Credentials);
        const imgUrls: object[] = [];
        //
        // Initalizin upload params (bucket name, file content and filename or key)
        //
        const uploadParams = {
                           Bucket: config.s3Params.bucketName,
                           Body: null,
                           Key: null
                         };

        //
        // Iterating over the set or 'rates' or 'factors' we are going to apply to the original
        // image size, to resize and store them in S3
        //
        for ( const key in config.imgDpis.rates) {
          if ( config.imgDpis.rates.hasOwnProperty(key) ) {
              //
              // Calculating new image width and height
              //
              const newWidth = Math.round(width * config.imgDpis.rates[key]);
              const newHeight = Math.round(height * config.imgDpis.rates[key]);
              //
              // Getting the URL or key for the new image
              //
              const imgUrl = 'images/' + key + '/' + entity + '/' + picture['name'];
              const absImgUrl = 'https://' + config.s3Params.bucketName + '.s3.amazonaws.com/public/' + imgUrl;
              imgUrls.push({density: key, url: imgUrl, absUrl: absImgUrl });
              // console.log('width: ' + newWidth + ', height: ' + newHeight);
              //
              // Saving the image in the server disk (should we use await sharp ?)
              //
              /*
              sharp(picture['data']).resize(newWidth, newHeight).toFile('public/' + imgUrl, (error, info) => {
                if (error) {
                  console.log(error);
                }
              });
              */

              //
              // Saving the image in the S3 bucket (should we use await sharp ?). Image key or path must begin
              // with '/public', because it is required by some client apps ...
              //
              await sharp(picture['data']).resize(newWidth, newHeight).toBuffer().then(buffer => {
                                      uploadParams.Body = buffer;
                                      uploadParams.Key = 'public/' + imgUrl;
                                      s3.putObject(uploadParams, (error, data) => {
                                        if (error) {
                                          console.log('ERROR 2: ' + error);
                                        } else {
                                          console.log('Image ' + imgUrl + ' uploaded to S3 succesfully');
                                        }
                                      });
              });

          }
        }
        // console.log(imgUrls);
        return new Promise (function (resolve, reject) {
          if (imgUrls.length !== config.imgDpis.count ) {
            reject({status: 'ERROR', data: imgUrls});
          } else {
            resolve({status: 'OK', data: imgUrls});
          }
        });
      }

     /**
      * resizeAndSavePicture: Takes an input image as a buffer, resizes it to the specified width and height (if required)
      * and saves it to S3 at the specified path (key)
      *
      * @param picture: A Buffer object containing the image data
      *
      * @param key: the path (including the picture name) in the S3 bucket
      *
      * @param resize: true or false, to say if the image will be (or won't be) resized
      *
      * @param width: The input image's width (if rezise was required)
      *
      * @param height: the input image's height (if resize was required)
      *
      * @returns: an object with the result of the operation
      */

      static async resizeAndSavePicture(picture: Buffer,
                                        key: string,
                                        resize: boolean,
                                        width: number,
                                        height: number): Promise<boolean> {
        const s3Credentials = config.s3Params.credentials;
        s3Credentials.accessKeyId = process.env.ACCESSKEY;
        s3Credentials.secretAccessKey = process.env.SECRETKEY;
        const s3 = new AWS.S3(s3Credentials);
        const uploadParams = {
          Bucket: config.s3Params.bucketName,
          Body: null,
          Key: null
        };
        let saved: boolean = false;
        if (resize) {
            //
            // We must resize the input picture to the specified (width,height) dimensions, before saving it to S3
            //
            sharp(picture).resize(width, height).toBuffer().then(buffer => {
              uploadParams.Body = buffer;
              uploadParams.Key = key;
              s3.putObject(uploadParams, (error, data) => {
                if (error) {
                  console.log(error);
                } else {
                  saved = true;
                }
              });
            });
        } else {
            //
            // No resizing was required. Save the picture to S3 as it is.
            //
            uploadParams.Body = picture;
            uploadParams.Key = key;
            s3.putObject(uploadParams, (error, data) => {
              if (error) {
                console.log(error);
              } else {
                saved = true;
              }
            });
        }
        return new Promise (function (resolve, reject) {
            resolve(saved);
        });

      }
}
