import {
        EndpointResponse,
        EndpointResponseStatus,
       } from '../models';

import { Connection, createConnection, ConnectionManager, getConnectionManager } from 'typeorm';
//import { Request } from 'express';
import config from '../../../config/app.config';
import dbConnectionParams from '../../../config/db.config';


/**
 *  This class is intended to provide HELPERS functions for different purposes
 *
 */

export abstract class  MiscHelpers {

    /**
     * setResponse: Sets up the generic EndpointResponse object to be used for all REST
     *              responses sent by ALL CONTROLLERS
     *
     * @param status: OK, ERROR or FATAL
     * @param code: the HTTP response code
     * @param data: a JSON object which can contain data
     * @param errors: an array of errors
     * @param message: a string containing a generic message about the result
     * @returns: and EndpointResponse object
    */

    
    static setResponse(status: EndpointResponseStatus, code: number, data: object, errors: object[], message: string): EndpointResponse {
        const endpointResponse = new EndpointResponse();
        endpointResponse.setStatus(status);
        endpointResponse.setCode(code);
        if ( data !== null ) {
            endpointResponse.setData(data);
        }
        if (errors) {
            endpointResponse.setErrors(errors);
        }
        endpointResponse.setMessage(message);
        return endpointResponse;
    }

    /**
      *
      * Returns a Database Connection. Checks if a connection pool already
      * exists and returns the 'default' connection, otherwise creates a Connection Pool
      * and returns it
      * 
      * @returns: an ORM Connection Object
      * (NOTICE: This functions uses ormconfig.json as the connection input parameters)
    */


    static async getDbConnection(): Promise<Connection> {

        const CONNECTION_NAME = 'default';
        let connection: Connection;
        const connectionManager: ConnectionManager = getConnectionManager();
        if (connectionManager.has(CONNECTION_NAME)) {
          connection = await connectionManager.get(CONNECTION_NAME);
          connection = connectionManager.get(CONNECTION_NAME);   
          if (!connection.isConnected) {
                connection = await connection.connect();
          }
          console.log('USING an existing CONNECTION POOL : ' + connectionManager.get(CONNECTION_NAME));
        } else {
          console.log('CONNECTION POOL DOES NOT EXIST or NOT AVAILABLE ... CREATING A CONNECTION POOL...');
          connection = await createConnection();
        }
        return connection;
    }

    /**
      *
      * Returns a Database Connection. Checks if a connection pool already
      * exists and returns the 'default' connection, otherwise creates a Connection Pool
      * and returns it
      * 
      * @returns: an ORM Connection Object
      * (NOTICE: This functions gets all connection params from the application config
      * but the db password, which is gotten from AWS Secret Manager Service)
      * 
      connection = await createConnection({
            "type": "postgres",
            "host": "nationsushiprojectinstance.cwtjehp6sej3.us-east-2.rds.amazonaws.com",
            "port": 5432,
            "username": "nation_sushi_users_usr",
            "password": "my secret password",
            "database": "nation_sushi_users_db"
        });
    */
    

     static async getSecureDbConnection(): Promise<Connection> {
      
      // const CONNECTION_NAME = 'default';
      let connection: Connection;
      const connectionManager: ConnectionManager = getConnectionManager();
      // if (connectionManager.has(CONNECTION_NAME)) {
      if (connectionManager.connections.length > 0) {
        connection = connectionManager.get(connectionManager.connections[0].name);
        //connection = connectionManager.get(CONNECTION_NAME);
        if (!connection.isConnected) {
              connection = await connection.connect();
        }
        console.log('USING an existing CONNECTION: ' + connectionManager.connections[0].name);
        // console.log('USING an existing CONNECTION: ' + CONNECTION_NAME);
        console.log("# of connections available: ", connectionManager.connections.length);
      } else {
        console.log('CONNECTION POOL DOES NOT EXIST or NOT AVAILABLE ... CREATING A CONNECTION');
        const secretManagerResponse = await MiscHelpers.getDbPassword();
        const dbSecretKey = JSON.parse(secretManagerResponse.toString()); 
        //console.log("Secret Pair", dbSecretKey);
        //console.log("Secret password", dbSecretKey.dbsecret)
        const connectionParams = JSON.parse(JSON.stringify(dbConnectionParams));
        //HERE WE MUST SET THE PASSWORD INTO connectionParams(object) before
        //creating the connection
        //connectionParams.password = 'the password in ormconfig.json';
        connectionParams.password = dbSecretKey.dbsecret;
        //console.log(connectionParams);
        connectionManager.create(connectionParams);
        //connection = connectionManager.get(CONNECTION_NAME);
        connection = connectionManager.get(connectionManager.connections[0].name);
        if (!connection.isConnected) {
          connection = await connection.connect();
        }
        console.log("# of connections available: ", connectionManager.connections.length)
      }
      return connection;
   }

    /*
      This function should get a sucurely stored db password from AWS
    */
    static async getDbPassword() : Promise<String> {
      
      const AWS = require('aws-sdk');
      const region = "us-east-1";
      const secretName = "dev/nsushi/postgresql";
      let secret;
      let decodedBinarySecret;

      // Create a Secrets Manager client
      const client = new AWS.SecretsManager({
          region: region
      });

      
      const secretKey = await client.getSecretValue({SecretId: secretName}).promise();
      if (secretKey?.SecretString) {
        return secretKey?.SecretString;
      } else {
        let buff = new Buffer(secretKey.SecretBinary, 'base64');
        decodedBinarySecret = buff.toString('ascii');
        return decodedBinarySecret;
      }
    }

    /**
     * getCountryCode: tries to get the Country Code for a given phone number
     *
     * @param phone: phone number
     * @returns: the phone number's Country Code, otherwise null
    */

    static getCountryCode(phone: string): string {
      var Phonenumber = require ('awesome-phonenumber');
      return Phonenumber(phone).getRegionCode();
    }

    /**
     * getRounded2Dec
     * 
     * @param number : the number to be rounded
     * @returns : the number rounded up to 2 decimals
    */
    static getRounded2Dec (number: number) {
      // return Math.round((number + 0.00001) * 100) / 100;
      return Math.round((number + Number.EPSILON) * 100) / 100;
    }

    /**
     * fillLWithChar: Left pad a string with "number" of "chars"
     * 
     * @param number : the number to be filled with zeroes
     * @param char : the char to be used for filling the number
     * @param limit  : the final string lenght
     * @returns : the number rounded up to 2 decimals
    */

    static fillLWithChar (number: number, char: string, limit: number) {
      let sNumber = String(number);
      while (sNumber.length < limit) {
        sNumber = char + sNumber;
      }
      return sNumber;
    }
    /**
     * callApi : to call an Api for a particular purpose, for example, validate if a reservation instance exists
     * 
     * @param req : the Request object received by the calling controller
     * @param service: the name of the API, let's say orders, countries, reservations, restaurants ...
     * @param filter : a particular search filter to be sent to the API 
     * @returns : and EndpointResponse object
    */
    static callApi = async (
                     reqContext: string,
                     reqService: string,
                     reqResource: string,
                     reqHeaders: object,
                     reqMethod: string,
                     reqData: object
                    ) : Promise<object>  => {
      //
      // Importing 'axios' for making API calls  
      //
      const axios = require('axios');
      //
      // Service input param can hold a value like these: orders, restaurants, countres, reservations, etc
      //  
      // url example = 
      let url = config.apis.EXECENV[process.env.EXECENV][reqService][reqContext] + reqResource;
      console.log(url);
      // Calling the API The response is an object with the same structure
      // as EndpointResponse, so we send it directly to the client without calling
      // MisHelpers.setResponse
      //
      let httpCode;        
      const axiosConfig = {
                        method: reqMethod,
                        url: url,
                        headers: reqHeaders
      };
      if (reqData) {
        axiosConfig['data'] = reqData;
      }
      const propertiesWithErrors = [];
      const axiosResponse = await axios(axiosConfig)
      .then(response => {
              return response['data'];
      })
      .catch(error =>{
        let errMsg = reqService + ' API : ';
        if (error['response']) {
          // If the error was generated by the called controller or service, there is an EndpointResponse
          // object coming in the response, from which we can extract the error message. Otherwise, we take
          // the error from error['response']
          if (error['response']['data']) {
            errMsg = errMsg + error['response']['data']['message'];
          } else {
            errMsg = errMsg + error['response']['statusText'];
          }     
          httpCode = error['response']['status'];
          propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
          return MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, errMsg);
        } else {
          httpCode = 500;
          errMsg = errMsg + error['code']
          propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
          return MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
        }
      });
      return new Promise (function (resolve, reject) {
        resolve(axiosResponse);
      });
    }

    /**
     * handleAPIError : to deal with FATAL errors when calling a particular
     * API
     * 
     * @param message : something of the kind 'Users PUBLIC API :' to know
     *                  which API issued the error
     * @param error: the specific error object 
     * @returns : and EndpointResponse object
    */
    /*
    static handleAPIError (message,error)  {
      let httpCode;
      let propertiesWithErrors = [];
      let errMsg = message;
      if (error['response']) {
        errMsg = errMsg + error['response']['statusText'];
        if (error['response']['data']) {
          errMsg = errMsg + error['response']['data']['message'];
        }
        httpCode = error['response']['status'];
      } else {
        httpCode = 500;
        errMsg = errMsg + error['code']
      }
      propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
      return MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
    }
    */

   static handleAPIError (message,error)  {
    let httpCode;
    let propertiesWithErrors = [];
    let errMsg = message;

    if (error['response']) {
      // If the error was generated by the called controller or service, there is an EndpointResponse
      // object coming in the response, from which we can extract the error message. Otherwise, we take
      // the error from error['response']
      if (error['response']['data']) {
        if (error['response']['data']['message']) {
          errMsg = errMsg + error['response']['data']['message'];
        } else {
          errMsg = errMsg + error['response']['statusText'];
        }
      } else {
        errMsg = errMsg + error['response']['statusText'];
      }     
      httpCode = error['response']['status'];
      propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
      return MiscHelpers.setResponse(EndpointResponseStatus.ERR, httpCode, null, propertiesWithErrors, errMsg);
    } else {
      httpCode = 500;
      errMsg = errMsg + error['code']
      propertiesWithErrors.push({ property: 'none', errors: [errMsg]});
      return MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, propertiesWithErrors, errMsg);
    }
  }
}

