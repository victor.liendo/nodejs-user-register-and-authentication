/**
 *  This class is intended to provide helpers for search functions
 */

// operator inside search filters will be translated to PostgreSQL operators
const postgresOperators =
{
    eq: '=',
    ne: '!=',
    gt: '>',
    gte: '>=',
    lte: '<=',
    lt: '<',
    lk: 'like',
    ilk: 'ilike',
    is: 'is',
    isnot: 'is not',
    in: 'in',
    notin: 'not in'
};

export abstract class  SearchHelpers {

    /**
     * getWhereCondition
     *
     * @param suffix: a suffix to be appended to databases fields which are part of the
     *                search condition, for example: 'co' (for the countries entity)
     *                15-10-2019: 'suffix' is not being used anymore, as expression
     *                            string has been extended to 4 elements, and the first
     *                            one contains the entity the filter will be applied to
     * 
     * @param condition: a logical expression inside a search filter, for example
     *                   category:ctg_name:eq:USA (4 elements)
     *
     * @returns: an object with properties compliant with TypeORM QueryBuilder 'where' condition
     *           example {expression: 'co.name = :name', value: {name: 'USA'}}
     */
    static getWhereCondition(suffix: string, expression: string): object {
        let status = false;
        let typeOrmExpression = '';
        let typeOrmValue = {};
        let whereCondition = {};
        const expressionElements = expression.split(':');
        // A search or filter expresion is valid if it has 4 elements 
        // (1-entity, 2-property or attribute, 3-operator, 4-value)
        if (expressionElements.length === 4) {
            // A search expression is valid if it has a valid Postgres operator
            if (expressionElements[2] in postgresOperators) {
                 /*
                 if (suffix !== '') {
                            typeOrmExpression = suffix  + '.'
                  }
                 */
                status = true;
                typeOrmExpression = typeOrmExpression + expressionElements[0] + '.';
                typeOrmExpression = typeOrmExpression + expressionElements[1];
                typeOrmExpression = typeOrmExpression + ' ';
                typeOrmExpression = typeOrmExpression +  postgresOperators[expressionElements[2]];
                typeOrmExpression = typeOrmExpression + ' ';
                //
                // When the operator is not one of these (is, isnot), the "where" condition
                // will be like this: 'country.name = :name, {name: 'USA'}'
                //
                if (expressionElements[2] !== 'is'
                &&  expressionElements[2] !== 'isnot'
                &&  expressionElements[2] !== 'in'
                &&  expressionElements[2] !== 'notin') {   

                        typeOrmExpression = typeOrmExpression + ':';
                        typeOrmExpression = typeOrmExpression +  expressionElements[1];

                        if (expressionElements[2] === 'lk' || expressionElements[2] === 'ilk' ) {
                            typeOrmValue = { [expressionElements[1]] : '%' + expressionElements[3] + '%' };
                        } else {
                            typeOrmValue = { [expressionElements[1]] :  expressionElements[3] }
                        }

                        whereCondition = {expression: typeOrmExpression, value: typeOrmValue}
                } else {
                        //
                        // Otherwise, it will be like this : 'country.name is null'
                        //                                or 'country.name isnot null'
                        //                                or 'country.name in ('ITALY','FRANCE')'
                        //                                or 'country.cty_id in (53,54)'
                        //              
                        typeOrmExpression = typeOrmExpression + expressionElements[3];
                        whereCondition = {expression: typeOrmExpression};
                }
            }
        }
        // console.log(status);
        // return ({ status: status, expression: typeOrmExpression, value: typeOrmValue });
        whereCondition['status'] = status;
        //console.log(whereCondition);
        return (whereCondition);
    }

    /**
     * getSortCondition
     *
     * @param suffix: a suffix to be appended to databases fields which are part of the
     *                sort condition, for example: 'co' (for the countries entity)
     *                15-10-2019: 'suffix' is not being used anymore, as order
     *                            string has been extended to 3 elements, and the first
     *                            one contains the entity the sort condition will be used for
     * 
     * @param condition: a sort condition
     *                   name:ASC
     *
     * @returns: an object with properties compliant with TypeORM QueryBuilder 'sort' condition
     *           for example: { category.ctg_name: ASC}
     */

    static getSortCondition(suffix: string, order: string): object {
        let status = false;
        let typeOrmValue = {};
        const orderElements = order.split(':');
        // An order condition is valid if it has 3 elements
        // (1-entity, 2-property or attribute, 3-ORDER)
        if (orderElements.length === 3) {
            // A sort condition is valid if it's value is in (ASC, DESC)
            if (orderElements[2] === 'ASC' || orderElements[2] === 'DESC') {

                typeOrmValue = { [orderElements[0] + '.' + orderElements[1]] : orderElements[2] };
                status = true;
            }
        }
        // console.log(status);
        return ({ status: status, value: typeOrmValue });
    }
}
