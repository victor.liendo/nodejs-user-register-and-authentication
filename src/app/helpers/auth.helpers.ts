import { Config } from '@foal/core';
import jwt = require('jsonwebtoken');
import randomstring = require('randomstring');
import { getRepository } from 'typeorm';
import { Challengecode, Token, User } from '../entities';
import { Category as CategoryChallengeCode, Dunits } from '../entities/challengecode.entity';
import { Category as CategoryToken } from '../entities/token.entity';
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader('./config/app.properties');
const messagesLang = 'spa';
const axios = require('axios');
// import { Category } from '../entities/token.entity';
// const moment = require('moment');

/**
 * Class Helpers: This is a static class to hold application utilities or helpers for the rest of the app
 *  Can't be instantiated because it is an abstract class
 */
export abstract class  SecurityHelpers {

    /**
     * getToken: Returns a signed token
     *
     * @param payload: the data items to be include in the signature
     * @param category: the type of the token (session, registration, password recovery)
     * @returns: a signed token
     */

    static getToken(payload: object, category: CategoryToken, useragent: string): string {
        // We can specify the algorithm to protect the token (HS256 by default)
        // this way: { expiresIn: '1h', algorithm: HS384 }
        //          { expiresIn: '1h', algorithm: HS512}
        const authToken = jwt.sign(
            payload,
            Config.get<string>('settings.jwt.secretOrPublicKey'),
            { expiresIn: '1h' }
            );
        getRepository(Token).save({
            token: authToken,
            // tslint:disable-next-line: object-literal-sort-keys
            status: true,
            // createdat: moment(new Date()),
            // updatedat: moment(new Date()),
            // tslint:disable-next-line: object-literal-shorthand
            category: category,
            // tslint:disable-next-line: object-literal-shorthand
            useragent: useragent
          });

        return authToken;
    }

    /**
     * getRandomCode: Returns a signed token
     *
     * @param userId: the user for whom the ramdo code will be generated
     * @param category: code type (user registration, password recovery)
     * @returns: a ramdom code, or an empty string in case of failure
     *
     * 1.- Find the user involved
     * 2.- Delete any previous code sent to that user for the same purpose (CategoryChallengeCode)
     * 3.- Generate a new random code, until it doesn't exist in the database, and return it
     *     We set the code's duration time by reading params from FoalTS configuration (default.json)
     */

    // static async getRamdomCode(userId: number, category: CategoryChallengeCode): Promise<string> {
    static async getRamdomCode(userEmail: string, category: CategoryChallengeCode): Promise<string> {
        let randomCode = '';
        // const user = await getRepository(User).findOne({id: userId});
        const user = await getRepository(User).findOne({email: userEmail});
        if (user) {
            await getRepository(Challengecode).delete({ createdby: {id: user.id},
                                                        category: category
                                                      });
            while (true) {
                randomCode = randomstring.generate({
                    length: 6,
                    charset: 'alphanumeric',
                    capitalization: 'lowercase'
                });
                if (!await getRepository(Challengecode).findOne({
                                                                    code: randomCode
                                                                })
                ) {
                        // tslint:disable-next-line: no-string-literal
                        const duration = Config.get<string>('challengecodes')['expiration']['userregistration'];
                        const challengeCode = new Challengecode();
                        challengeCode.code = randomCode;
                        challengeCode.category = category;
                        challengeCode.status = true;
                        challengeCode.createdby = user;
                        challengeCode.updatedby = user;
                        // tslint:disable-next-line: no-string-literal
                        switch (duration['units']) {
                            case 'hours': {
                                challengeCode.durationunits = Dunits.HRS;
                                break;
                            }
                            case 'mins': {
                                challengeCode.durationunits = Dunits.MIN;
                                break;
                            }
                            case 'days': {
                                challengeCode.durationunits = Dunits.DYS;
                                break;
                            }
                            case 'months': {
                                challengeCode.durationunits = Dunits.MTH;
                                break;
                            }
                            default: {
                                challengeCode.durationunits = Dunits.YRS;
                                break;
                            }
                        }
                        // tslint:disable-next-line: no-string-literal
                        challengeCode.duration = duration['time'];
                        await getRepository(Challengecode).save(challengeCode);
                        break;
                }
            }
        }
        return randomCode;
    }

    /**
     * Checks for a valida Google authentication Token:
     *
     * Proceeds as follows:
     * 1.- Call the Google validatin endpoint
     * 2.- Inspect the object received for:
     *  a.- Is not empty
     *  b.- Contains a property called 'response'
     *  c.- 'response' property contains a property called 'data'
     *  d.- 'data' property contains a property called 'issued_to' -> Validation has happened successfully
     *  e.- 'data' property contains a property called 'error_description'
     *
     * It's likely to happen that's enough to check result['response']['status'] === 200 for a
     * successful token validation ...
     *
     * @param url : The Google API url for the token validation endpoint (configured in ./config/default.json)
     * @param token : The token to be validated
     *
     * @returns : an object of the form : { status: <boolean>, message: <string> }
     */

    static async isAValidGoogleTokenByHttp(url, token: string): Promise<object> {
        const result = await axios.get(url + token)
        .then(function getHttpResponse(response: object) {
            return (response);
        }).
        catch(function getHttpResponse(response: object) {
            return (response);
        });
        if (result) {
            if (result['response']) {
                // Checking for OK (token succesfully authenticated) will likely be enough just by inspecting
                // result['response']['status'] === 200
                if (result['response']['data']) {
                    if (result['response']['data']['issued_to']) {
                        return ({status: true, message: properties.get(messagesLang + '-messages.google.auth.isok')});
                    } else {
                        if (result['response']['data']['error_description']) {
                            return ({status: false, message: result['response']['data']['error_description']});
                        } else {
                            return ({status: false, message: properties.get(messagesLang + '-messages.google.auth.failedandnoerrorincluded')});
                        }
                    }
                } else {
                    return ({status: false, message: properties.get(messagesLang + '-messages.google.auth.failedandnodataincluded')});
                }
            } else {
                return ({status: false, message: properties.get(messagesLang + '-messages.google.auth.failedandnoresponseobjectincluded')});
            }
        } else {
            return ({status: false, message: properties.get(messagesLang + '-messages.google.auth.failedandnohttpresponseincluded')});
        }
    }

    /**
     * NOT USED YET
     * @param token: The token to be validated
     */

    static async isAValidGoogleTokenByOauth(token: string): Promise<object> {

        const CLIENT_ID = '1009751610328-p4d5jdjltolcmmo4s6k6bujaiij9c6p7.apps.googleusercontent.com';
        const {OAuth2Client} = require('google-auth-library');
        const client = new OAuth2Client(CLIENT_ID);

        async function verify() {
            const ticket = await client.verifyIdToken({
            idToken: token,
            audience: CLIENT_ID
            });
            const payload = ticket.getPayload();
            const userid = payload['sub'];
        }

        const result = await verify().then(function isOk() {
            return ({status: true, message: properties.get(messagesLang + '-messages.google.auth.isok')});
        }).catch(function isNotOk(error) {
            return ({status: false, message: properties.get(messagesLang + '-messages.google.auth.failed')});
        });
        return (result);
    }

    /**
     * Decodes a token generated by this backend
     * 
     * @param token : The token to be validated by the Backend
     *
     */
    static async getDecodedToken(token: string): Promise<object> {
        var result = {};
        const actualToken = token.replace('Bearer ', '');
        const dbToken = await getRepository(Token).findOne({ token: token });
        if (dbToken) {
            if (dbToken.status) {
                try {
                    const decodedToken = await jwt.verify(actualToken, Config.get<string>('settings.jwt.secretOrPublicKey'));
                    result = {
                        status: true,
                        decodedToken : decodedToken,
                        message: 'NSUSHI token has been validated succesfully'
                    };
                } catch (error) {
                    result = {
                        status: false,
                        decodedToken: {},
                        message: error.message
                    };
                }
            } else {
                result = {
                    status: false,
                    decodedToken: {},
                    message: properties.get(messagesLang + '-messages.token.notfound.invalid')
                };
            }
        } else {
            result = {
                status: false,
                decodedToken: {},
                message: properties.get(messagesLang + '-messages.token.notfound.invalid')
            };
        }
        return result;
    }

}
